import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws java.lang.Exception {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();

        for (int i = 0; i < t; i++) {
            int n = input.nextInt();
            int x = input.nextInt();
            int y = input.nextInt();

            for (int j = x; j < n; j += x) {
                if (j % y != 0) {
                    System.out.print(j + " ");
                }
            }
            System.out.println();
        }
    }
}


//Wypisz wszystkie liczby ai podzielne przez x i niepodzielne przez y, gdzie 1 < ai < n < 100000.
//
//Wejście
//Najpierw w oddzielnej linii t liczba przypadków testowych następnie w kolejnych t liniach liczby n x y.
//
//Wyjście
//W kolejnych t liniach oddzielone pojedynczym odstępem liczby spełniające warunki zadania wypisane od najmniejszej do największej.



/*
Wejście:
2                                         t - ile przypadkow testowych
7 2 4                                     n x y       n - górna granica, x-dzielą się przez tą liczbę y-nie dzieli się przez tą liczbe
35 5 12                                   n x y
Wyjście:
2 6
5 10 15 20 25 30
*/